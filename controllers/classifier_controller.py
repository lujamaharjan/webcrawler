
from flask.views import MethodView
from flask import render_template, redirect, request
from utils.classifier import NaiveBayesClassifierUtil

class Classifier(MethodView):

    def get(self):
        return render_template("classifier/index.html")
    
    def post(self):
        form_data = request.form
        text = form_data.get('news_text')
        nb_classifier = NaiveBayesClassifierUtil()
        news_class = nb_classifier.nb_classify(text)
        return render_template('classifier/index.html', text=text, news_class=news_class)
    