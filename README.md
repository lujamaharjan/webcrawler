
# INTRODUCTION
Web crawl is the vertical search engine that search papers of the coventry university written by memeber of intelligent health care system.

## Running Program
1. Install virtualenv package using  <br />`pip install virtualenv`
1. Create Virtualenv  using <br /> `virtualenv venv`
1. Activate Virtualenv using command <br /> `venv/scripts/activate` in windows os. you can use it for your own way if you are in mac or linux.
1. Install all the project dependencies in requirements.txt file <br/> `pip install -r requirements.txt`
1. Now, up and run program using `python app.py` command. go into favoriate browser and hit localhost:5000 in search bar. Ola its working :)
<br/>
Note: I have used python3.11 version. I suggest you to use the same version.



